# Pickup-Pup
Casual mobile game with dogs.

## Creator: [Sam Luangkhot](http://www.samluangkhot.com/)

## Engineers
- [Isaiah Mann](http://isaiahmann.com/)
- [Grace Barrett-Snyder](http://gracebarsny.com/)
- James Hostetler

## Specs
- Unity: **v5.5.0f3**
- Language: **C#**
