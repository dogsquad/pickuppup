﻿/*
 * Author: Isaiah Mann
 * Description: Generic superclass to handle loading from resources
 */

[System.Serializable]
public class ResourceLoader 
{
	protected const string AUDIO_DIR = "Audio";
	protected const string JSON_DIR = "JSON";
	protected const string SPRITES_DIR = "Sprites";
	protected const string DEFAULT = "Default";
	protected const string GAME_DATA = "GameData";
	protected const string EXPORT = "Export";
	protected const string SHEET = "Sheet";

	protected const char JOIN_CHAR = '_';

}
