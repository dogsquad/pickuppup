﻿/*
 * Authors: Grace Barrett-Snyder, Isaiah Mann
 * Description: Details a type of currency
 */

public enum CurrencyType 
{
    Coins,
    DogFood,
    HomeSlots,
    Time,
	SpecialObject,
    None,

}
